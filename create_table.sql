CREATE TABLE "test-topic"(
   create_ts timestamp with time zone UNIQUE NOT NULL,
   update_ts timestamp with time zone,
   c1 integer,
   c2 text
);
